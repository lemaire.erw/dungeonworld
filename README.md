# Install

1. Go to the setup page and choose **Game Systems**.
2. Click the **Install System** button, and paste in this manifest link:
    * Foundry 0.5.x: [https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/raw/master/system.json](https://gitlab.com/asacolips-projects/foundry-mods/dungeonworld/raw/master/system.json)
3. Create a Game World using the Dungeon World system.

Compatible with FoundryVTT 0.3.x

# Description

Build campaigns in the Dungeon World RPG using Foundry VTT!

![Screenshot of the player character sheet](https://i.imgur.com/pBDNyoh.png)